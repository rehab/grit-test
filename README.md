# grit-test

Just trying [grit](https://gitlab.com/gitlab-org/ci-cd/runner-tools/grit/-/tree/master) out.


```bash
✗ export GITLAB_TOKEN=""
✗ cat terraform.tfvars
access_key = ""
secret_key = ""
✗ terraform init
✗ terraform validate
✗ terraform plan
✗ terraform apply
✗ terraform destroy
```
