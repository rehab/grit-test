module "single-ec2-shell-runner" {
  source = "./grit/modules/test"

  manager_provider  = "ec2"
  capacity_provider = "none"

  gitlab_project_id         = 51077827
  gitlab_runner_description = "grit-runner"
  gitlab_runner_tags        = ["grit-rehab"]
}

provider "aws" {
  region = "us-east-1"
  access_key = var.access_key
  secret_key = var.secret_key
}
